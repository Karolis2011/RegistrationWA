<?php
namespace LoginBundle\Security;

use LoginBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\HttpFoundation\Session\Session;

class UserVoter extends Voter
{
    private $session;

    const VIEW = 'view';
    const EDIT = 'edit';
    const PASSWORD = 'password';

    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, array(self::VIEW, self::EDIT, self::PASSWORD))) {
            return false;
        }

        if (!$subject instanceof User) {
            return false;
        }
        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        if (!$user instanceof User) { //Logged in user check
            return false;
        }

        switch ($attribute) {
            case self::VIEW:
                if ($subject == $user) { //User can view there self
                    return true;
                }
                return false;
            case self::EDIT:
                if ($subject == $user) { //User can edit there self
                    return true;
                }
                return false;
            case self::PASSWORD:
                if ($subject == $user) { //User can chandge there password
                    return true;
                }
                return false;
        }

        throw new \LogicException('This code should not be reached!');
    }
}
