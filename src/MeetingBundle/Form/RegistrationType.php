<?php

namespace MeetingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class RegistrationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('participantName')
            ->add('studentName')
            ->add('notes')
            ->add('meeting', EntityType::class, array(
                'class' => 'MeetingBundle:Meeting',
                'choice_label' => 'name'
                ))
            ->add('user', EntityType::class, array(
                'class' => 'LoginBundle:User',
                'choice_label' => 'fullname',
                'placeholder' => 'Undefined User',
                'empty_data'  => null
                ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MeetingBundle\Entity\Registration'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'meetingbundle_registration';
    }


}
