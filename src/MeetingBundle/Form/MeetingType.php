<?php

namespace MeetingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class MeetingType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('startTime', null, array(
                'date_widget' => 'single_text',
                'time_widget' => 'text'
            ))
            ->add('endTime', null, array(
                'date_widget' => 'single_text',
                'time_widget' => 'text'
            ))
            ->add('limit')
            ->add('host', EntityType::class, array(
                'class' => 'LoginBundle:User',
                'choice_label' => 'fullname'
                ))
            ->add('groups', EntityType::class, array(
                'class' => 'MeetingBundle:MeetingGroup',
                'choice_label' => 'note',
                'multiple' => true,
                'expanded' => true,
                ))
            ->add('parent', EntityType::class, array(
                'class' => 'MeetingBundle:Meeting',
                'choice_label' => 'name',
                'required'    => false,
                'placeholder' => 'None',
                'empty_data'  => null
                ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MeetingBundle\Entity\Meeting'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'meetingbundle_meeting';
    }


}
