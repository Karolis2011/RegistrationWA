<?php

namespace MeetingBundle\Controller;

use MeetingBundle\Entity\MeetingGroup;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Meetinggroup controller.
 *
 * @Route("meetinggroup")
 */
class MeetingGroupController extends Controller
{
    /**
     * Lists all meetingGroup entities.
     *
     * @Route("/", name="meetinggroup_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $meetingGroups = $em->getRepository('MeetingBundle:MeetingGroup')->findAll();

        return $this->render('MeetingBundle:MeetingGroup:index.html.twig', array(
            'meetingGroups' => $meetingGroups,
        ));
    }

    /**
     * Creates a new meetingGroup entity.
     *
     * @Route("/new", name="meetinggroup_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $meetingGroup = new Meetinggroup();
        $form = $this->createForm('MeetingBundle\Form\MeetingGroupType', $meetingGroup);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($meetingGroup);
            $em->flush($meetingGroup);

            return $this->redirectToRoute('meetinggroup_show', array('id' => $meetingGroup->getId()));
        }

        return $this->render('MeetingBundle:MeetingGroup:new.html.twig', array(
            'meetingGroup' => $meetingGroup,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a meetingGroup entity.
     *
     * @Route("/{id}", name="meetinggroup_show")
     * @Method("GET")
     */
    public function showAction(MeetingGroup $meetingGroup)
    {
        $deleteForm = $this->createDeleteForm($meetingGroup);

        return $this->render('MeetingBundle:MeetingGroup:show.html.twig', array(
            'meetingGroup' => $meetingGroup,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing meetingGroup entity.
     *
     * @Route("/{id}/edit", name="meetinggroup_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, MeetingGroup $meetingGroup)
    {
        $deleteForm = $this->createDeleteForm($meetingGroup);
        $editForm = $this->createForm('MeetingBundle\Form\MeetingGroupType', $meetingGroup);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('meetinggroup_edit', array('id' => $meetingGroup->getId()));
        }

        return $this->render('MeetingBundle:MeetingGroup:edit.html.twig', array(
            'meetingGroup' => $meetingGroup,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a meetingGroup entity.
     *
     * @Route("/{id}", name="meetinggroup_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, MeetingGroup $meetingGroup)
    {
        $form = $this->createDeleteForm($meetingGroup);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($meetingGroup);
            $em->flush($meetingGroup);
        }

        return $this->redirectToRoute('meetinggroup_index');
    }

    /**
     * Creates a form to delete a meetingGroup entity.
     *
     * @param MeetingGroup $meetingGroup The meetingGroup entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(MeetingGroup $meetingGroup)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('meetinggroup_delete', array('id' => $meetingGroup->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
