<?php

namespace MeetingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use MeetingBundle\Entity\Registration;

class RegistrationMangementController extends Controller
{
    /**
     * @Route("/code", name="registration_code")
     */
    public function codeAction(Request $request)
    {
        $t = $this->get('translator');
        $code = $request->request->get("code");
        if ($code != null) {
            $repository = $this->getDoctrine()->getRepository('MeetingBundle:MeetingGroup');
            $gr = $repository->findOneByCode($code);
            if ($gr != null) {
                $session = $request->getSession();
                $session->set('meetinggroup_id', $gr->getId());
                if ($session->get('participant_name') != null) {
                    //We can send them to select a time
                    $this->addFlash(
                        'success',
                        [
                            'title' => $t->trans('MeetingBundle:Registration:InfoUpdate:UpSucc:Title'),
                            'message' => $t->trans('MeetingBundle:Registration:InfoUpdate:UpSucc:Title'),
                        ]
                    );
                    return $this->redirectToRoute('meeting_view', array('id' => $gr->getId()));
                } else {
                    //Need to get there name
                    $this->addFlash(
                        'info',
                        [
                            'title' => $t->trans('MeetingBundle:Registration:CodeEntry:InfoUpdate:Title'),
                            'message' => $t->trans('MeetingBundle:Registration:CodeEntry:InfoUpdate:Msg'),
                        ]
                    );
                    return $this->redirectToRoute('registration_updateinfo');
                }
            } else {
                $this->addFlash(
                    'warning',
                    [
                        'title' => $t->trans('MeetingBundle:Registration:CodeEntry:RegNotFound:Title'),
                        'message' => $t->trans('MeetingBundle:Registration:CodeEntry:RegNotFound:Msg'),
                    ]
                );
            }
        } else {
            $code = "";
            $this->addFlash(
                'info',
                [
                    'title' => $t->trans('MeetingBundle:Registration:CodeEntry:RegInfo:Title'),
                    'message' => $t->trans('MeetingBundle:Registration:CodeEntry:RegInfo:Msg'),
                ]
            );
        }
        return $this->render('MeetingBundle:Registration:code.html.twig', array(
            'oldcode' => $code,
        ));
    }

    /**
     * @Route("/reg/update", name="registration_updateinfo")
     */
    public function infoUpdateAction(Request $request)
    {
        $t = $this->get('translator');
        $session = $request->getSession();
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('MeetingBundle:MeetingGroup');
        $gr = $repository->find($session->get('meetinggroup_id'));
        if ($gr == null) {
            return $this->createNotFoundException('Your meeting nolonger exist.');
        }
        if ($session->get('participant_name') != null) {
            return $this->redirectToRoute('meeting_view', array('id' => $gr->getId()));
        }
        $pname = $request->request->get("name");
        $sname = $request->request->get("sname");
        if (!empty($pname) && !empty($sname)){
            $session->set('participant_name', $pname);
            $session->set('student_name', $sname);
            $this->addFlash(
                'success',
                [
                    'title' => $t->trans('MeetingBundle:Registration:InfoUpdate:UpSucc:Title'),
                    'message' => $t->trans('MeetingBundle:Registration:InfoUpdate:UpSucc:Msg'),
                ]
            );
            return $this->redirectToRoute('meeting_view', array('id' => $gr->getId()));
        } else if ($request->isMethod('POST')) {
            $this->addFlash(
                'danger',
                [
                    'title' => $t->trans('MeetingBundle:Registration:InfoUpdate:FailEmpty:Title'),
                    'message' => $t->trans('MeetingBundle:Registration:InfoUpdate:FailEmpty:Msg'),
                ]
            );
        }
        return $this->render('MeetingBundle:Registration:info.html.twig', array(
        ));
    }
    /**
     * @Route("/registration/register", name="registration_reg")
     */
    public function registerAction(Request $request)
    {
        $t = $this->get('translator');
        $session = $request->getSession();
        $em = $this->getDoctrine()->getManager();
        $regrepository = $em->getRepository('MeetingBundle:Registration');
        $merepository = $em->getRepository('MeetingBundle:Meeting');
        $meetingid = intval($request->request->get('id'));
        $pname = $session->get("participant_name");
        $sname = $session->get("student_name");
        if (!$meetingid || empty($pname) || empty($sname)){
            throw $this->createNotFoundException('Invalid data');
        }
        $meeting = $merepository->find($meetingid);
        if($meeting == null) {
            throw $this->createNotFoundException('Meeting not found');
        }
        foreach ($meeting->getRegistrations() as $registration) {
            if($registration->getParticipantName() == $pname) {
                return $this->json(array(
                    "success" => false,
                    "message" => $t->trans("MeetingBundle:Meeting:View:RegDup:Msg"),
                ));
            }
        }
        if($meeting->getOpenPositions() <= 0){
            return $this->json(array(
                    "success" => false,
                    "message" => $t->trans("MeetingBundle:Meeting:View:NoOpen:Msg"),
                ));
        }
        $reg = new Registration;
        $reg->setMeeting($meeting)
            ->setParticipantName($pname)
            ->setStudentName($sname);
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $user = $this->getUser();
            $reg->setUser($user);
        }
        $em->persist($reg);
        $em->flush();
        return $this->json(array(
            "success" => true,
            "message" => $t->trans("MeetingBundle:Meeting:View:RegSuc:Msg"),
        ));
    }
}
