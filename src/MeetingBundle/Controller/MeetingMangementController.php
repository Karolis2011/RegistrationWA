<?php

namespace MeetingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\ExpressionLanguage\Expression;
use Symfony\Component\HttpFoundation\Request;

class MeetingMangementController extends Controller
{
    /**
     * @Route("/mgv/{id}", name="meeting_view")
     */
    public function viewAction(Request $request, $id)
    {
        $t = $this->get('translator');
        $session = $request->getSession();
        $em = $this->getDoctrine()->getManager();
        $megrepository = $em->getRepository('MeetingBundle:MeetingGroup');
        $meetingg = $megrepository->find($id);
        if ($meetingg == null) {
            $meetingg = $megrepository->findOneByCode($id);
            if($meetingg == null) {
                throw $this->createNotFoundException('This meeting does not exist.');
            }
        }
        if (empty($session->get("participant_name")) || empty($session->get("student_name"))) {
            $this->addFlash(
                'info',
                [
                    'title' => $t->trans('MeetingBundle:Registration:CodeEntry:InfoUpdate:Title'),
                    'message' => $t->trans('MeetingBundle:Registration:CodeEntry:InfoUpdate:Msg'),
                ]
            );
            $session->set('meetinggroup_id', $meetingg->getId());
            return $this->redirectToRoute('registration_updateinfo');
        }
        $meetings = $meetingg->getMeetings();
        $hosts = array();
        foreach ($meetings as $meeting) {
            $host = $meeting->getHost();
            if(!in_array($host, $hosts)){
                $hosts[] = $host;
            }
        }
        return $this->render('MeetingBundle:Meeting:view.html.twig', array(
            'meetingg' => $meetingg,
            'meetings' => $meetings,
            'hosts' => $hosts,
        ));
    }
}
