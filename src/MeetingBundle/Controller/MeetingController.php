<?php

namespace MeetingBundle\Controller;

use MeetingBundle\Entity\Meeting;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Meeting controller.
 *
 * @Route("meeting")
 */
class MeetingController extends Controller
{
    /**
     * Lists all meeting entities.
     *
     * @Route("/", name="meeting_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $meetings = $em->getRepository('MeetingBundle:Meeting')->findAll();

        return $this->render('MeetingBundle:Meeting:index.html.twig', array(
            'meetings' => $meetings,
        ));
    }

    /**
     * Creates a new meeting entity.
     *
     * @Route("/new", name="meeting_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $this->denyAccessUnlessGranted("ROLE_HOST");
        $meeting = new Meeting();
        $form = $this->createForm('MeetingBundle\Form\MeetingType', $meeting);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($meeting);
            $em->flush($meeting);

            return $this->redirectToRoute('meeting_show', array('id' => $meeting->getId()));
        }
        return $this->render('MeetingBundle:Meeting:new.html.twig', array(
            'meeting' => $meeting,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a meeting entity.
     *
     * @Route("/{id}", name="meeting_show")
     * @Method("GET")
     */
    public function showAction(Meeting $meeting)
    {
        $this->denyAccessUnlessGranted("view", $meeting);
        $deleteForm = $this->createDeleteForm($meeting);
        return $this->render('MeetingBundle:Meeting:show.html.twig', array(
            'meeting' => $meeting,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Converts meeting to individual meetings.
     *
     * @Route("/{id}/convert", name="meeting_convert")
     * @Method("GET")
     */
    public function convertAction(Meeting $meeting)
    {
        $this->denyAccessUnlessGranted("edit", $meeting);
        $em = $this->getDoctrine()->getManager();
        $diff = $meeting->getEndTime()->diff($meeting->getStartTime());
        $diffmins = $diff->i + $diff->h * 60;
        $interval = round($diffmins / $meeting->getLimit());
        $intervati = new \DateInterval("PT" . $interval . "M");
        if ($meeting->getLimit() > 0) {
            if (count($meeting->getChildren()) > 0) {
                throw $this->createNotFoundException("Invalid meeting data: has children");
            }
            if (count($meeting->getRegistrations()) > 0) {
                throw $this->createNotFoundException("Invalid meeting data: has registrations");
            }
            $st = $meeting->getStartTime();
            for ($i = 0; $i < $meeting->getLimit(); $i++) {
                $nemeet = new Meeting();
                $nemeet
                    ->setName($meeting->getName() . " #" . ($i + 1))
                    ->setStartTime(clone $st)
                    ->setEndTime(clone $st->add($intervati))
                    ->setHost($meeting->getHost())
                    ->setGroup($meeting->getGroups()->toArray())
                    ->setLimit(1)
                    ->setParent($meeting);
                $em->persist($nemeet);
            }
            $em->flush();
        } else {
            throw $this->createNotFoundException("Invalid meeting data: limit");
        }

        return $this->redirectToRoute('meeting_show', array('id' => $meeting->getId()));
    }

    /**
     * Displays a form to edit an existing meeting entity.
     *
     * @Route("/{id}/edit", name="meeting_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Meeting $meeting)
    {
        $this->denyAccessUnlessGranted("edit", $meeting);
        $deleteForm = $this->createDeleteForm($meeting);
        $editForm = $this->createForm('MeetingBundle\Form\MeetingType', $meeting);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('meeting_edit', array('id' => $meeting->getId()));
        }

        return $this->render('MeetingBundle:Meeting:edit.html.twig', array(
            'meeting' => $meeting,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a meeting entity.
     *
     * @Route("/{id}", name="meeting_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Meeting $meeting)
    {
        $this->denyAccessUnlessGranted("edit", $meeting);
        $form = $this->createDeleteForm($meeting);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($meeting);
            $em->flush($meeting);
        }

        return $this->redirectToRoute('meeting_index');
    }

    /**
     * Creates a form to delete a meeting entity.
     *
     * @param Meeting $meeting The meeting entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Meeting $meeting)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('meeting_delete', array('id' => $meeting->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
