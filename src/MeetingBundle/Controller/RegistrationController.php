<?php

namespace MeetingBundle\Controller;

use MeetingBundle\Entity\Registration;
use MeetingBundle\Entity\Meeting;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Registration controller.
 *
 * @Route("registration")
 */
class RegistrationController extends Controller
{
    /**
     * Lists all registration entities.
     *
     * @Route("/", name="registration_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $registrations = $em->getRepository('MeetingBundle:Registration')->findAll();

        return $this->render('MeetingBundle:Registration:index.html.twig', array(
            'registrations' => $registrations,
        ));
    }

    /**
     * Lists all registration entities.
     *
     * @Route("/me/{id}", name="registration_index_filter")
     * @Method("GET")
     */
    public function indexFilerAction(Meeting $meeting)
    {
        $em = $this->getDoctrine()->getManager();
        return $this->render('MeetingBundle:Registration:index.html.twig', array(
            'meeting' => $meeting,
            'registrations' => $meeting->getAllRegistrations(),
        ));
    }

    /**
     * Creates a new registration entity.
     *
     * @Route("/new", name="registration_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $this->denyAccessUnlessGranted("ROLE_HOST");
        $registration = new Registration();
        $form = $this->createForm('MeetingBundle\Form\RegistrationType', $registration);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($registration);
            $em->flush($registration);

            return $this->redirectToRoute('registration_show', array('id' => $registration->getId()));
        }

        return $this->render('MeetingBundle:Registration:new.html.twig', array(
            'registration' => $registration,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a registration entity.
     *
     * @Route("/{id}", name="registration_show")
     * @Method("GET")
     */
    public function showAction(Registration $registration)
    {
        $this->denyAccessUnlessGranted("view", $registration);
        $deleteForm = $this->createDeleteForm($registration);

        return $this->render('MeetingBundle:Registration:show.html.twig', array(
            'registration' => $registration,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing registration entity.
     *
     * @Route("/{id}/edit", name="registration_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Registration $registration)
    {
        $this->denyAccessUnlessGranted("edit", $registration);
        $deleteForm = $this->createDeleteForm($registration);
        $editForm = $this->createForm('MeetingBundle\Form\RegistrationType', $registration);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('registration_edit', array('id' => $registration->getId()));
        }

        return $this->render('MeetingBundle:Registration:edit.html.twig', array(
            'registration' => $registration,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a registration entity.
     *
     * @Route("/{id}", name="registration_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Registration $registration)
    {
        $this->denyAccessUnlessGranted("edit", $registration);
        $form = $this->createDeleteForm($registration);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($registration);
            $em->flush($registration);
        }

        return $this->redirectToRoute('registration_index');
    }

    /**
     * Creates a form to delete a registration entity.
     *
     * @param Registration $registration The registration entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Registration $registration)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('registration_delete', array('id' => $registration->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
