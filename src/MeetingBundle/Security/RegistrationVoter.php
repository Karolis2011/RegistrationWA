<?php
namespace MeetingBundle\Security;

use LoginBundle\Entity\User;
use MeetingBundle\Entity\Registration;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\HttpFoundation\Session\Session;

class RegistrationVoter extends Voter
{
    private $session;

    const VIEW = 'view';
    const EDIT = 'edit';

    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, array(self::VIEW, self::EDIT))) {
            return false;
        }

        if (!$subject instanceof Registration) {
            return false;
        }
        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        if (!$user instanceof User) { //User must be logged in for futher checks to work
            return false;
        }
        switch ($attribute) {
            case self::VIEW:
                if($subject->getUser() == $user){ //User that executed registration can also look at info.
                    return true;
                }
                if($subject->getMeeting()->getHost() == $user){ //Meeting host can look at info too.
                    return true;
                }
                return false;
            case self::EDIT:
                if($subject->getUser() == $user){ //User that executed registration can also look at info.
                    return true;
                }
                if($subject->getMeeting()->getHost() == $user){ //Meeting host can look at info too.
                    return true;
                }
                return false;
        }

        throw new \LogicException('This code should not be reached!');
    }
}
