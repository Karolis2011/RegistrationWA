<?php

namespace MeetingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Meeting
 *
 * @ORM\Table(name="meeting")
 * @ORM\Entity(repositoryClass="MeetingBundle\Repository\MeetingRepository")
 */
class Meeting
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startTime", type="datetime")
     */
    private $startTime;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endTime", type="datetime")
     */
    private $endTime;

    /**
     * @ORM\ManyToOne(targetEntity="LoginBundle\Entity\User", inversedBy="meetings")
     * @ORM\JoinColumn(name="host_id", referencedColumnName="id")
     */
    private $host;

    /**
     * @ORM\OneToMany(targetEntity="MeetingBundle\Entity\Registration", mappedBy="meeting")
     */
    private $registrations;

    /**
     * @ORM\ManyToMany(targetEntity="MeetingBundle\Entity\MeetingGroup", inversedBy="meetings")
     */
    private $groups;

    /**
     * @ORM\OneToMany(targetEntity="Meeting", mappedBy="parent")
     */
    private $children;

    /**
     * @ORM\ManyToOne(targetEntity="Meeting", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;

    /**
     * @ORM\Column(name="reglimit", type="integer")
     */
    private $limit;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Meeting
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->registrations = new \Doctrine\Common\Collections\ArrayCollection();
        $this->groups = new \Doctrine\Common\Collections\ArrayCollection();
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set host
     *
     * @param \LoginBundle\Entity\User $host
     *
     * @return Meeting
     */
    public function setHost(\LoginBundle\Entity\User $host = null)
    {
        $this->host = $host;

        return $this;
    }

    /**
     * Get host
     *
     * @return \LoginBundle\Entity\User
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * Add registration
     *
     * @param \MeetingBundle\Entity\Registration $registration
     *
     * @return Meeting
     */
    public function addRegistration(\MeetingBundle\Entity\Registration $registration)
    {
        $this->registrations[] = $registration;

        return $this;
    }

    /**
     * Remove registration
     *
     * @param \MeetingBundle\Entity\Registration $registration
     */
    public function removeRegistration(\MeetingBundle\Entity\Registration $registration)
    {
        $this->registrations->removeElement($registration);
    }

    /**
     * Get registrations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRegistrations()
    {
        return $this->registrations;
    }

    /**
     * Get all registrations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAllRegistrations()
    {
        $reg = $this->registrations->toArray();
        foreach ($this->children as $hi) {
            $reg = array_merge($reg, $hi->getAllRegistrations());
        }
        return $reg;
    }

    /**
     * Set group
     *
     * @param $group
     *
     * @return Meeting
     */
    public function setGroup($group)
    {
        if(!is_array($group)) {
            $group = array($group);
        }
        $this->groups = $group;

        return $this;
    }
    

    /**
     * Add group
     *
     * @param \MeetingBundle\Entity\MeetingGroup $group
     *
     * @return Meeting
     */
    public function addGroup(\MeetingBundle\Entity\MeetingGroup $group)
    {
        $this->groups[] = $group;

        return $this;
    }

    /**
     * Remove group
     *
     * @param \MeetingBundle\Entity\MeetingGroup $group
     */
    public function removeGroup(\MeetingBundle\Entity\MeetingGroup $group)
    {
        $this->groups->removeElement($group);
    }

    /**
     * Get groups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * Set startTime
     *
     * @param \DateTime $startTime
     *
     * @return Meeting
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * Get startTime
     *
     * @return \DateTime
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * Set endTime
     *
     * @param \DateTime $endTime
     *
     * @return Meeting
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;

        return $this;
    }

    /**
     * Get endTime
     *
     * @return \DateTime
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * Set limit
     *
     * @param integer $limit
     *
     * @return Meeting
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * Get limit
     *
     * @return integer
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * Add child
     *
     * @param \MeetingBundle\Entity\Meeting $child
     *
     * @return Meeting
     */
    public function addChild(\MeetingBundle\Entity\Meeting $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \MeetingBundle\Entity\Meeting $child
     */
    public function removeChild(\MeetingBundle\Entity\Meeting $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set parent
     *
     * @param \MeetingBundle\Entity\Meeting $parent
     *
     * @return Meeting
     */
    public function setParent(\MeetingBundle\Entity\Meeting $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \MeetingBundle\Entity\Meeting
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Get remaining registration slots open
     *
     * @return int
     */
    public function getOpenPositions()
    {
        if($this->limit == -1){
            return 1;
        }
        if(count($this->children) > 0) {
            $childpos = 0;
            foreach ($this->children as $hi) {
                $childpos += $hi->getOpenPositions();
            }
            return $childpos;
        }
        return $this->limit - count($this->registrations);
    }

}
