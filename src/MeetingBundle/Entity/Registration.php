<?php

namespace MeetingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Registration
 *
 * @ORM\Table(name="registration")
 * @ORM\Entity(repositoryClass="MeetingBundle\Repository\RegistrationRepository")
 */
class Registration
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="participantName", type="string", length=255, nullable=true)
     */
    private $participantName;

    /**
     * @var string
     *
     * @ORM\Column(name="studentName", type="string", length=255, nullable=true)
     */
    private $studentName;

    /**
     * @var string
     *
     * @ORM\Column(name="notes", type="text", nullable=true)
     */
    private $notes;

    /**
     * @ORM\ManyToOne(targetEntity="MeetingBundle\Entity\Meeting", inversedBy="registrations")
     * @ORM\JoinColumn(name="meeting_id", referencedColumnName="id")
     */
    private $meeting;

    /**
     * @ORM\ManyToOne(targetEntity="LoginBundle\Entity\User", inversedBy="registrations")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set participantName
     *
     * @param string $participantName
     *
     * @return Registration
     */
    public function setParticipantName($participantName)
    {
        $this->participantName = $participantName;

        return $this;
    }

    /**
     * Get participantName
     *
     * @return string
     */
    public function getParticipantName()
    {
        return $this->participantName;
    }

    /**
     * Set notes
     *
     * @param string $notes
     *
     * @return Registration
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set meeting
     *
     * @param \MeetingBundle\Entity\Meeting $meeting
     *
     * @return Registration
     */
    public function setMeeting(\MeetingBundle\Entity\Meeting $meeting = null)
    {
        $this->meeting = $meeting;

        return $this;
    }

    /**
     * Get meeting
     *
     * @return \MeetingBundle\Entity\Meeting
     */
    public function getMeeting()
    {
        return $this->meeting;
    }

    /**
     * Set user
     *
     * @param \LoginBundle\Entity\User $user
     *
     * @return Registration
     */
    public function setUser(\LoginBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \LoginBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set studentName
     *
     * @param string $studentName
     *
     * @return Registration
     */
    public function setStudentName($studentName)
    {
        $this->studentName = $studentName;

        return $this;
    }

    /**
     * Get studentName
     *
     * @return string
     */
    public function getStudentName()
    {
        return $this->studentName;
    }
}
